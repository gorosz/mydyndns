defmodule Mydyndns do
  @moduledoc """
  Documentation for Mydyndns.
  """
  def main do
    Mydyndns.Agent.start_link("")
    ip_address = PublicIp.get

    unless Mydyndns.Agent.get == ip_address do
      {:telegram, ip_address} |>
        create_message |>
        Notifier.send
      Mydyndns.Agent.update(ip_address)
    end
  end

  defp create_message({:telegram, ip}) do
    %Mydyndns.Messages.Telegram{telegram_id: 68_153_069, msg: ip}
  end
end
