defmodule Mydyndns.Messages.Telegram do
  @moduledoc ""
  @enforce_keys [:telegram_id]
  defstruct [:telegram_id, :msg]
end

defimpl Notifier, for: Mydyndns.Messages.Telegram do
  def send(message) do
    Nadia.send_message(message.telegram_id, message.msg)
  end
end
