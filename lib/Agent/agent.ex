defmodule Mydyndns.Agent do
  use Agent

  @moduledoc ""
  @name __MODULE__

  def start_link(_opts) do
    Agent.start_link(fn -> "" end, name: @name)
  end

  def get() do
    Agent.get(@name, fn state -> state end)
  end

  def update(new_state) do
    Agent.update(@name, fn state -> new_state end)
  end
end
