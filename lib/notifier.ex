defprotocol Notifier do
  def send(message)
end
