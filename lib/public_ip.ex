defmodule PublicIp do
  @moduledoc ""
  @api_url "https://api.ipify.org/?format=json"

  def get() do
    with {:ok, response} <- HTTPoison.get(@api_url, []) do
      {:ok, %{"ip" => ip_address}} = Poison.Parser.parse(response.body)
      ip_address
    end
  end

end
