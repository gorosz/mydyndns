defmodule PublicIpTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  setup_all do
    # ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes", "fixture/custom_cassettes")
    HTTPoison.start
  end

  test "can parse ip addres" do
    use_cassette "response_mocking" do
      ip = PublicIp.get
      assert ip == "1.1.1.1"
    end
  end
end
