defmodule MydyndnsAgentTest do
  use ExUnit.Case

  setup do
    {:ok, agent} = start_supervised(Mydyndns.Agent)
    :ok
  end

  test "start with initial state" do
    assert Mydyndns.Agent.get() == ""
  end

  test "hold updated state" do
    Mydyndns.Agent.update("new_ip")
    assert Mydyndns.Agent.get() == "new_ip"
  end
end
